<?php
namespace Paynova\response;
/**
 *
 * service: UpdateOrderLineItems
 * type: 	response
 *
 * This class will be used in response to RequestUpdateOrderLineItems
 * Hold only read-properties
 *
 * @package Paynova/response
 * @copyright Paynova 2015
 */

class ResponseUpdateOrderLineItems extends Response {
	
	/**
	 * @see response/Response::__construct()
	 */
	public function __construct() {
		parent::__construct(array(
          "transactionId","riskAssessment"=>"Paynova\\response\\model\\RiskAssessment"  
        ));
	}
	
    /**
	 * transactionId getter
	 * Paynova's unique transaction id for the refund. This property is only returned for successful 
	 * operations.
	 * @return string transactionId
	 */
	public function transactionId() {  return $this->setOrGet(__FUNCTION__,null); }

	/**
	 * riskAssessment getter
	 * @return string riskAssessment
	 */
	public function riskAssessment($object = null) { 
		if($object != null)Util::validateObject($object, "Paynova\\response\\model\\RiskAssessment");
		return $this->setOrGet(__FUNCTION__,$object); 
	}
	
}
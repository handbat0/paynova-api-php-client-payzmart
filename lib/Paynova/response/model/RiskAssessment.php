<?php
namespace Paynova\response\model;

use Paynova\model\Instance;

/**
 * class RiskAssessment
 * part of service: 	see response/model/RiskAssessment
 *
 *
 * @package Paynova/response/model
 * @copyright Paynova 2014
 *
 */
class RiskAssessment extends Instance {
	
	/**
	 * @see model/Instance::__construct()
	 */
	public function __construct() {
		parent::__construct(array(
            "mode","result"
		));
	}
	
	/**
	 * mode getter
	 * The four-digit mode  of the card.
	 * @return string mode
	 */
	public function mode() { return $this->setOrGet(__FUNCTION__,null); }
	
	/**
	 * result getter
	 * The two-digit result  of the card.
	 * @return string result
	 */
	public function result() { return $this->setOrGet(__FUNCTION__,null); }
}
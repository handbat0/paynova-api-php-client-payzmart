<?php
namespace Paynova\response;
/**
 *
 * service: 	UpdateMerchantReference
 * type: 		response
 *
 * An object of this class will be created by RequestUpdateMerchantReference from the API REST call response
 * Hold only read-properties
 *
 * @package Paynova/request
 * @copyright Paynova 2014
 *
 */
class ResponseUpdateMerchantReference extends Response {
	/**
	 * @see response/Response::__construct()
	 */
	public function __construct() {
		parent::__construct(array());
	}
}
<?php
namespace Paynova\request;

use Paynova\http\HttpConfig;
use Paynova\util\Util;


/**
 * service: 	Update Order LineItems 
 * type: 		request
 * 
 * The Remove Customer Profile Card service is used to remove a stored card from a customer profile.
 * 
 * @package Paynova/request
 * @copyright Paynova 2014
 *
 */
class RequestUpdateOrderLineItems extends Request {
	
	public function __construct($http = null) {
		parent::__construct(array(
			"operations"=>"Paynova\\request\\model\\Operations",
			"transactionId","orderId"
			),
			array(
				"operations","transactionId","orderId"
			),
			"orders/{orderId}/transactions/{transactionId}/lineItems",
			$http
		);
	}

	/**
	 * Do the RemoveUpdateOrderLineItems  API request - ReponseRemoveUpdateOrderLineItems is returned
	 * 
	 * @throws PaynovaExceptionRequiredPropertyMissing
	 * @throws PaynovaExceptionHttp if exception occured when contacting server
	 * @throws PaynovaExceptionConfig
	 * @param HttpConfig $httpConfig (optional)
	 * @return ReponseUpdateOrderLineItems
	 */

	public function request(HttpConfig $httpConfig = null) {
		return parent::doRequest("PATCH",$httpConfig);
	}

     /**
	 * operations getter
	 * The Update Order service is used to update the order lines of an existing order. 
	 * operations.
	 * @return string operations
	 * @return ReponseUpdateOrderLineItems
	 */
    public function operations($object = null) { 
		if($object != null)Util::validateObject($object, "Paynova\\request\\model\\Operations");
		return $this->setOrGet(__FUNCTION__,$object); 
	}

    /**
	 * transactionId getter
	 * Paynova's unique transaction id for the refund. This property is only returned for successful 
	 * operations.
	 * @return string transactionId
	 */
	public function transactionId() {  return $this->setOrGet(__FUNCTION__,null); }
	
	/**
	 * orderId setter/getter
	 * The unique identifier (GUID) that you received from Paynova in the response from Create Order.
	 * @param string $value (optional) used when setting
	 * @return ReponseUpdateOrderLineItems or string orderId
	 */
	public function orderId($value = null) {  return $this->setOrGet(__FUNCTION__,$value); }
}
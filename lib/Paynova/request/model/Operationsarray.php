<?php
namespace Paynova\request\model;

use Paynova\model\Instance;
use Paynova\util\Util;

/**
 * class Operationsarray
 * part of service: 	see Paynova/request/model/Operationsarray
 * 
 * 
 * @package Paynova/request/model
 * @copyright Paynova 2014
 *
 */
class Operationsarray extends Instance {

	/**
	 * @see model/Instance::__construct()
	 */
	public function __construct() {
		parent::__construct(array(
			"operationType","lineItemId",
			"lineItem"=>"Paynova\\request\\model\\LineItem"
		));
	}
	
	/**
	 * operationType setter/getter
	 * The Valid Operationsarray.
	 * @param string $value (optional) used when setting
	 * @return Operationsarray or string operationType
	 */
	public function operationType($value = null) { return $this->setOrGet(__FUNCTION__,$value); }
	
	/**
	 * lineItemId setter/getter
	 * The carrier's line Item Id.
	 * @param string $value (optional) used when setting
	 * @return Operationsarray or int lineItemId
	 */
	public function lineItemId($value = null) { return $this->setOrGet(__FUNCTION__,$value); }
	
	/**
	 * lineItem setter/getter
	 * The line item ID.
	 * @param boolean $bool (optional) used when setting
	 * @return Operationsarray or boolean lineItem
	 */
	
	public function lineItem($object = null) { 
		if($object != null)Util::validateObject($object, "Paynova\\request\\model\\LineItem");
		return $this->setOrGet(__FUNCTION__,$object); 
	}
		
}
<?php
namespace Paynova\request;

use Paynova\http\HttpConfig;
use Paynova\util\Util;


/**
 * service: 	Update Merchant Reference
 * type: 		request
 * 
 * The Remove Customer Profile Card service is used to remove a stored card from a customer profile.
 * 
 * @package Paynova/request
 * @copyright Paynova 2014
 *
 */
class RequestUpdateMerchantReference extends Request {
	
	public function __construct($http = null) {
		parent::__construct(array(
				"orderId","merchantReference"
			),
			array(
				"orderId","merchantReference"
			),
			"orders/{orderId}",
			$http
		);
		
	}

	/**
	 * Do the RemoveUpdateMerchantReference API request - ReponseRemoveUpdateMerchantReference is returned
	 * 
	 * @throws PaynovaExceptionRequiredPropertyMissing
	 * @throws PaynovaExceptionHttp if exception occured when contacting server
	 * @throws PaynovaExceptionConfig
	 * @param HttpConfig $httpConfig (optional)
	 * @return ReponseUpdateMerchantReference
	 */

	public function request(HttpConfig $httpConfig = null) {
		return parent::doRequest("PATCH",$httpConfig);
	}

	/**
	 * orderId setter/getter
	 * The unique identifier (GUID) that you received from Paynova in the response from Create Order.
	 * @param string $value (optional) used when setting
	 * @return ReponseUpdateMerchantReferenceor string orderId
	 */
	public function orderId($value = null) {  return $this->setOrGet(__FUNCTION__,$value); }

	/**
	 * merchantReference setter/getter
	 * Your reference for the order.
	 * @param string $value (optional) used when setting
	 * @return ReponseUpdateMerchantReference or string orderId
	 */
	public function merchantReference($value = null) {  return $this->setOrGet(__FUNCTION__,$value); }
}
